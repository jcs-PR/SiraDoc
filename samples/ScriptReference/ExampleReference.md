# ExampleReference

To illustrate all avaliable code related CSS.

Code: `Inline code`

```cs
/**
 * Code comment.
 * @param args : Parameters array.
 */
Code block
```

| Name                    | Description                                                        |
|:------------------------|:-------------------------------------------------------------------|
| mGameDepth              | Distance as game origin depth.                                     |
| mDisplayGameDepthCamera | Display the camera depth.                                          |
| mGameCamColor           | The color of the camera depth.                                     |
| mFollowing              | Flag to check if currently the camera following the target object. |
| PositionOffset          | Offset the camera position from its' original position.            |
| SmoothTrack             | Flag to check if using smooth track, otherwise hard track.         |
