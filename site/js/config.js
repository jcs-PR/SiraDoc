/**
 * $File: config.js $
 * $Date: 2018-09-28 22:33:54 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2018 by Shen, Jen-Chieh $
 */

"use strict";

/* Manual */
const manual_name = '$manual_name';

/* Theme */
const header_color = '$header_color';
const arrow_color  = '$arrow_color';
const th_show      = $th_show;       // table header
const th_color     = '$th_color';    // table header color

/* Version */
const version_title_01 = '$version_title_01';
const version_title_02 = '$version_title_02';
const version_title_03 = '$version_title_03';
const version_title_04 = '$version_title_04';
const version_title_05 = '$version_title_05';
const version_title_06 = '$version_title_06';
const version_title_07 = '$version_title_07';

const version_num_01 = '$version_num_01';
const version_num_02 = '$version_num_02';
const version_num_03 = '$version_num_03';
const version_num_04 = '$version_num_04';
const version_num_05 = '$version_num_05';
const version_num_06 = '$version_num_06';
const version_num_07 = '$version_num_07';

/* Copyright */
const copyright_text = '$copyright_text';

/* Homepage Link */
const homepage_url  = '$homepage_url';   // Link to your homepage.
const homepage_text = '$homepage_text';  // Text represent your homepage.

/* Search Input Text */
const si_manual_placeholder = "Search manual...";
const si_api_placeholder    = "Search scripting...";

const si_input_size = 17;  // character unit.
