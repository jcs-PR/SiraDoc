#!/usr/bin/env node
/**
 * $File: util.js $
 * $Date: 2021-08-29 13:32:24 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2021 by Shen, Jen-Chieh $
 */

"use strict";

const fs = require("fs");
const path = require('path');

/*
 * Logger
 */
function info(msg) { console.log("[INFO] " + msg); }
function error(msg) { console.log("[ERROR] " + msg); }

/*
 * File System
 */
function mkdir(dest) { fs.mkdirSync(dest, { recursive: true }); }
function readFile(dest) { return fs.readFileSync(dest, 'utf8'); }
function writeFile(dest, content) { fs.writeFileSync(dest, content, { encoding: 'utf8' }); }
function copyFile(src, dest) {
  mkdir(path.dirname(dest));
  fs.copyFileSync(src, dest);
}

function isFile(src) { return fs.lstatSync(src).isFile(); }
function isDir(src) { return !isFile(src); }

function optTarget(ext) {
  let exts = [".css", ".js", ".html"];
  return isExtension(ext, exts);
}

function isMedia(ext) {
  let exts = [".png", ".jpg", ".gif", ".jpeg", ".webp", ".svg"];
  return isExtension(ext, exts);
}

function isExtension(ext, exts) {
  for (let index = 0; index < exts.length; ++index) {
    if (exts[index] === ext)
      return true;
  }
  return false;
}

/*
 * Path
 */

/** Return a section by INDEX of path (SRC). */
function pathIn(src, index) {
  let splits = src.split(path.sep);
  return splits[index];
}

/** Remove a section by INDEX from the path (SRC). */
function pathSplice(src, index) {
  let splits = src.split(path.sep);
  splits.splice(index, 1);
  return splits.join(path.sep);
}

/* Conversion Keywords */
const slashKey = "_sl_";
const spaceKey = "_sp_";  // Must be the same as server.

const obKey      = "_ob_";   // open bracket '('.
const cbKey      = "_cb_";   // close bracket ')'.
const ocbKey     = "_ocb_";  // open curly bracket '{'.
const ccbKey     = "_ccb_";  // close curly bracket '}'.
const osbKey     = "_osb_";  // open square bracket '['.
const csbKey     = "_csb_";  // close square bracket ']'.

const atKey      = "_at_";   // At key '@'.
const caretKey   = "_cr_";   // Caret key '^'.
const bqKey      = "_bq_";   // Back quote key '`'.
const tildeKey   = "_tl_";   // Tilde key '~'.
const hashKey    = "_hs_";   // Hash key '#'.
const dollarKey  = "_dl_";   // Dollar '$' key.
const percentKey = "_pc_";   // Percent '%' key.
const andKey     = "_and_";  // And '&' key.
const plusKey    = "_pl_";   // And '+' key.
const quoteKey   = "_qt_";   // Quote ' key.
const exclaimKey = "_ex_";   // Exclamation mark key '!'.

const periodKey    = "_pri_";  // Period '.' key.
const equalKey     = "_eq_";   // Equals ' =' key.
const commaKey     = "_cma_";  // Comma ',' key.
const semicolonKey = "_sc_";   // Semicolon ';' key.

/**
 * Apply conversion rules.
 * @param { string } rawStr : Unrefined string that have not apply
 * conversion rules.
 * @param { boolean } revert : Convert back.
 */
function applyConversionRule(rawStr, revert = false) {
  if (revert) {
    rawStr = rawStr.split(slashKey).join("/");
    rawStr = rawStr.split(spaceKey).join(" ");

    rawStr = rawStr.split(obKey).join("(");
    rawStr = rawStr.split(cbKey).join(")");
    rawStr = rawStr.split(ocbKey).join("{");
    rawStr = rawStr.split(ccbKey).join("}");
    rawStr = rawStr.split(osbKey).join("[");
    rawStr = rawStr.split(csbKey).join("]");

    rawStr = rawStr.split(atKey).join("@");
    rawStr = rawStr.split(bqKey).join("`");
    rawStr = rawStr.split(caretKey).join("^");
    rawStr = rawStr.split(tildeKey).join("~");
    rawStr = rawStr.split(hashKey).join("#");
    rawStr = rawStr.split(dollarKey).join("$");
    rawStr = rawStr.split(percentKey).join("%");
    rawStr = rawStr.split(andKey).join("&");
    rawStr = rawStr.split(plusKey).join("+");
    rawStr = rawStr.split(quoteKey).join("'");
    rawStr = rawStr.split(exclaimKey).join("!");

    rawStr = rawStr.split(periodKey).join(".");
    rawStr = rawStr.split(equalKey).join("=");
    rawStr = rawStr.split(commaKey).join(",");
    rawStr = rawStr.split(semicolonKey).join(";");
  } else {
    rawStr = rawStr.replace(/\//g, slashKey);
    rawStr = rawStr.replace(/ /g, spaceKey);

    rawStr = rawStr.replace(/\(/g, obKey);
    rawStr = rawStr.replace(/\)/g, cbKey);
    rawStr = rawStr.replace(/\{/g, ocbKey);
    rawStr = rawStr.replace(/\}/g, ccbKey);
    rawStr = rawStr.replace(/\[/g, osbKey);
    rawStr = rawStr.replace(/\]/g, csbKey);

    rawStr = rawStr.replace(/\@/g, atKey);
    rawStr = rawStr.replace(/\`/g, bqKey);
    rawStr = rawStr.replace(/\^/g, caretKey);
    rawStr = rawStr.replace(/\~/g, tildeKey);
    rawStr = rawStr.replace(/\#/g, hashKey);
    rawStr = rawStr.replace(/\$/g, dollarKey);
    rawStr = rawStr.replace(/\%/g, percentKey);
    rawStr = rawStr.replace(/\&/g, andKey);
    rawStr = rawStr.replace(/\+/g, plusKey);
    rawStr = rawStr.replace(/\'/g, quoteKey);
    rawStr = rawStr.replace(/\!/g, exclaimKey);

    rawStr = rawStr.replace(/\./g, periodKey);
    rawStr = rawStr.replace(/\=/g, equalKey);
    rawStr = rawStr.replace(/\,/g, commaKey);
    rawStr = rawStr.replace(/\;/g, semicolonKey);
  }
  return rawStr;
}

/*
 * Module Exports
 */
module.exports.info = info;
module.exports.error = error;

module.exports.mkdir = mkdir;
module.exports.readFile = readFile;
module.exports.writeFile = writeFile;
module.exports.copyFile = copyFile;

module.exports.isFile = isFile;
module.exports.isDir = isDir;

module.exports.optTarget = optTarget;
module.exports.isMedia = isMedia;

module.exports.pathSplice = pathSplice;
module.exports.pathIn = pathIn;

module.exports.slashKey = slashKey;
module.exports.applyConversionRule = applyConversionRule;
