#!/usr/bin/env node
/**
 * $File: custom.js $
 * $Date: 2021-08-30 16:03:37 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2021 by Shen, Jen-Chieh $
 */

"use strict";

/** A list of all possible configuration keywords */
const keywords = [
  'manual_name',
  'header_color',
  'arrow_color',
  'th_show',
  'th_color',
  'version_title_01',
  'version_title_02',
  'version_title_03',
  'version_title_04',
  'version_title_05',
  'version_title_06',
  'version_title_07',
  'version_num_01',
  'version_num_02',
  'version_num_03',
  'version_num_04',
  'version_num_05',
  'version_num_06',
  'version_num_07',
  'copyright_text',
  'homepage_url',
  'homepage_text',
];

function applyConfigKeywords(config, content) {
  for (let index = 0; index < keywords.length; ++index) {
    let keyword = keywords[index];
    let replacement = config[keyword];
    if (replacement === undefined || replacement === null)
      continue;
    content = content.replace('$' + keyword, replacement);
  }
  return content;
}

/*
 * Minify
 */
const options = {
  html: {
    removeAttributeQuotes: false,
  },
};

/*
 * Module Exports
 */
module.exports.options = options;

module.exports.applyConfigKeywords = applyConfigKeywords;
