# Change Log

All notable changes to this project will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.


## 0.5.0 (Unreleased)
> Released N/A

* N/A

## 0.4.0 (Unreleased)
> Released Sep 3, 2021

* Now accept empty tree.
* Add suport for media files.

## 0.3.0
> Released Aug 30, 2021

* Add simple line breaks options.
* Add minify to optimize the generated website.
* Add YAML configuration.
* Enhance CLI interface with logger utility functions.

## 0.2.0
> Released Aug 29, 2021

* Update image `favicon.ico`.
* Fix CROS issue.

## 0.1.0
> Released Aug 29, 2021

* Initial release.
