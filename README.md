[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Latest Release](https://gitlab.com/SiraDoc/SiraDoc/-/badges/release.svg)](https://gitlab.com/SiraDoc/SiraDoc/-/releases)
[![npm](https://img.shields.io/npm/v/@siradoc/siradoc?logo=npm&color=green)](https://www.npmjs.com/package/@siradoc/siradoc)
[![npm-dm](https://img.shields.io/npm/dm/@siradoc/siradoc.svg)](https://npmcharts.com/compare/@siradoc/siradoc?minimal=true)

# ⚓ ᏕiraDoc

[![pipeline status](https://gitlab.com/SiraDoc/SiraDoc/badges/master/pipeline.svg)](https://gitlab.com/SiraDoc/SiraDoc/-/commits/master)
[![coverage report](https://gitlab.com/SiraDoc/SiraDoc/badges/master/coverage.svg)](https://gitlab.com/SiraDoc/SiraDoc/-/commits/master)
[![dependencies Status](https://status.david-dm.org/gh/SiraDoc/siradoc.svg)](https://david-dm.org/SiraDoc/siradoc)

Static documentation generator for scripting manual

## Demo

Looking for a demo? A simple demo is available at https://siradoc.gitlab.io/demo/!

<p align="center">
  <img src="./etc/demo.png"/>
</p>

## 💾 Installing

### NPM

To install SiraDoc using npm, execute the following command:

```sh
$ npm install -g @siradoc/siradoc
```

[![NPM](https://nodei.co/npm/@siradoc/siradoc.png)](https://www.npmjs.com/package/@siradoc/siradoc)

## 🔨 How to use?

The command takes two arguments, `source` and `destination`

```sh
$ siradoc <source> <destination>
```

For example,

```sh
$ siradoc ./docs ./build
```

The file structure is below,

```
│   Manual.md
│   ScriptReference.md
│   siradoc.yml  (config file)
│
├───Manual
│       Manual_Page_01.md
│       Manual_Page_02.md
│
└───ScriptReference
        Reference_Page_01.md
        Reference_Page_02.md
```

## 🔧 Configuration

Configuration file should be named `siradoc.yml` in your documentation
directory.

```yaml
# Manual
manual_name: '[Manual Name]'

# Theme
header_color: '#222C37'
arrow_color: '#19E3B1'
th_show: true
th_color: '#A2F2DE'

# Version
version_title_01: '[Manual] Version: '
version_num_01: '0.0.1'

# Copyright
copyright_text: 'Copyright © [yyyy] [Group Name]. Built: [yyyy-mm-dd].'

# Link
homepage_url: 'https://gitlab.com/SiraDoc/SiraDoc'
homepage_text: 'SiraDoc.gitlab'
```

## 📝 TODO

- [ ] Add search capability
- [ ] Add test for CI

## License

This software uses other open-source components. For a full list, see the `LICENSE` file.

```
MIT License

Copyright (c) 2021 Jen-Chieh Shen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
